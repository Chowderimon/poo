import java.sql.SQLOutput;

public class Calculadora {
    private double primero, segundo, resultado;
    public double sumar(){
        return primero+segundo;
    }
    public double restar(){
        return primero-segundo;
    }
    public double multiplicar(){
        return primero*segundo;
    }
    public double dividir(){
        if (primero>segundo)
        return primero/segundo;
        else return segundo/primero;
    }
    public static void main(String args[]){
        Calculadora a = new Calculadora();
        a.primero = 5.0;
        a.segundo = 5.8;
        a.resultado = a.sumar();
        System.out.println(a.resultado);
        a.resultado = a.restar();
        System.out.println(a.resultado);
        a.resultado = a.dividir();
        System.out.println(a.resultado);
        a.resultado = a.multiplicar();
        System.out.println(a.resultado);
    }
}
